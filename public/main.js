'use strict'

function loadSavedCreds(){
  let storedKey = localStorage.getItem('apikey'),
      storedSecret = localStorage.getItem('apisecret')

  if(storedKey && storedSecret){
    $('#clearcreds').removeClass('disabled')
    writeLog('Loading API credentials from localStorage.')
    $('#apikey').val(storedKey),
    $('#apisecret').val(storedSecret)
  }       
}

function saveCreds(){
  let enteredKey = $('#apikey').val(),
      enteredSecret = $('#apisecret').val()

  localStorage.setItem('apikey', enteredKey)
  localStorage.setItem('apisecret', enteredSecret)

  $('#clearcreds').removeClass('disabled')

  writeLog('Saved API credentials to localStorage')

}

function clearCreds(){
  let storedKey = localStorage.getItem('apikey'),
      storedSecret = localStorage.getItem('apisecret')

  if(storedKey || storedSecret){
    localStorage.removeItem('apikey')
    localStorage.removeItem('apisecret')

    $('#apikey').val(''),
    $('#apisecret').val('')

    $('#clearcreds').addClass('disabled')
    writeLog('Cleared API credentials from localStorage.')    
  } else {
    writeLog('No stored credentials to clear!')
  }
}

function keysChanged(){
  // let enteredKey = $('#apikey').val(),
  //     enteredSecret = $('#apisecret').val(),
  //     storedKey = localStorage.getItem('apikey'),
  //     storedSecret = localStorage.getItem('apiSecret')

  // if(enteredKey && enteredSecret) { 
  //   if(enteredKey == storedKey && enteredSecret == storedSecret){
  //     $('#loadcreds').addClass('disabled')
  //   } else {
  //     $('#loadcreds').removeClass('disabled')

  //   }
  // }

}

function getExpiryTime(){
  return new Date().getTime() + (60 * 1000) // 1 min in the future
}

function submitOrder(symbol, orderQty, ordType, price){
  const verb = 'POST',
        path = '/api/v1/order',
        expires = getExpiryTime()
  
  let data = {
    symbol: symbol,
    orderQty: orderQty,
    ordType: ordType
  }

  if(ordType == 'Limit') { data.price = price }

  const postBody = JSON.stringify(data)
  writeLog(postBody)
  
  const signature = createSignature(API_SECRET, verb, path, expires, postBody)
  writeLog("request signature: <br />" + signature)
  
  const headers = {
    'content-type' : 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    'api-expires': expires,
    'api-key': API_KEY,
    'api-signature': signature
  }
  
  const requestOptions = {
    headers: headers,
    method: verb,
    body: postBody
  }

  fetch('https://cors-anywhere.herokuapp.com/https://testnet.bitmex.com' + path, requestOptions)
    .then(response => response.json())
    .then(jsonData => {
      writeLog(JSON.stringify(jsonData))  
    })
    .catch(err => {
          writeLog(err)
          console.log(err)
      }
  )
}

function createSignature(apiSecret, verb, path, expires, postBody){
  if(!postBody) { postBody = '' }
  const hmac = CryptoJS.algo.HMAC.create(CryptoJS.algo.SHA256, apiSecret)
    .update(verb + path + expires + postBody)
  return hmac.finalize()
}

function writeLog(message){
  let d = new Date(),
      ts = `[${d.toISOString()}] `,
      msg = ts + message + '<br />'
  $('#log-output').prepend(msg)
}

// start
$(document).ready(function(){
  writeLog('Initialising...')

  loadSavedCreds()

  $('#savecreds').on('click', function(e){
    e.preventDefault()
    saveCreds()
  })

  $('#clearcreds').on('click', function(e){
    e.preventDefault()
    if( ! $('#clearcreds').hasClass('disabled') ) {
      clearCreds()
    }
  })

  // var host = $('#host').val()

  // $('#host').on('change', function(e){
  //   host = $('#host').val()
  //   ws.refresh()
  // })

  initiateWebsocket()

})

var ws = {};

function initiateWebsocket(){
  let expires = getExpiryTime(),
      verb = 'GET',
      endpoint = '/realtime'

  ws = new ReconnectingWebSocket(`wss://www.bitmex.com${endpoint}`, null, {
    debug: true,
    automaticOpen: false
  })

  ws.onopen = function(){
    writeLog('Websocket connected.')
    // send auth message
    //  ,
    // request = { op: "authKeyExpires", args: [API_KEY, expires, signature.toString()]}
    // ws.send(JSON.stringify(request))
  }

  ws.onclose = function(){
    writeLog('Websocket disconnected.')
  }

  ws.onconnecting = function(){
    writeLog('Connecting websocket...')
  }

  ws.open()

  // send auth message
  let signature = createSignature(API_SECRET, verb, endpoint, expires),
      request = { op: "authKeyExpires", args: [API_KEY, expires, signature.toString()]}

  // writeLog("Ready to send authKeyExpires request:")
  // writeLog(JSON.stringify(request))
  // writeLog("Signature:")
  // writeLog(signature)
  // ws.send(JSON.stringify(request))
}

const API_KEY = "Sy-y8WOv-Aoimlxu3KBXPQQG"
const API_SECRET = "uuFdN_uQEdfWpNWFBnDf_V_9IRi9BWoVx7i0A4nQlOVIEHM7"
