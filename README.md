# bitmex-api-dashboard
A simple HTML + client-side JS interface for accessing the Bitmex API.

## TODO
- switching between testnet/live
- ~~Refactor doFetchStuff~~
  - ~~Extract signature code to function~~
  - ~~make reusable function for sending orders~~
- Authorise websockets to get user info
  - show user positions/orders etc
- set up order forms & JS for sending
- allow canceling of orders shown
